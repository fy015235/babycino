// This program tests the bug where the second variable in a multiple variable declaration is initialized incorrectly.
class TestBugG1 {
    public static void main(String[] args) {
        System.out.println(new Test().f());
    }
}

class Test {
    public int f() {
        // Zero-length variable list declaration
        int;
        return 42;
    }
}
